FROM fedora:31
LABEL description="Helper image to build exceptionfree-readfile library"

# Note this file is meant to be run from the TOP LEVEL `api` directory using `-f` option to docker
COPY . /root
WORKDIR /root

# Refresh DNF with latest versions
RUN dnf -y install which make rust cargo

# Install stack
RUN curl -sSL https://get.haskellstack.org/ | sh

# Install hlint
RUN stack install hlint

# Build app with profiling enabled
RUN make build test-all
