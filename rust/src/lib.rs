use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::ptr::null;

#[repr(C)]
pub struct ContentsOrError {
    err: *const c_char,
    contents: *const c_char,
}

// TODO: return length? safer reading on haskell side

#[no_mangle]
pub unsafe extern "C" fn read_file(path: *const c_char, debug: bool) -> *mut ContentsOrError {
    // Convert the cstring to a rust string
    if debug {
        println!("[libhaskell_exceptionfree_readfile] Attempting to read file from [{:?}]", CStr::from_ptr(path).to_str());
    }

    let path = match CStr::from_ptr(path).to_str() {
        Ok(v) => v,
        Err(e) => return Box::into_raw(
            Box::new(
                ContentsOrError {
                    err: CString::new(format!("Invalid path: {}", e))
                        .expect("Invalid path (printing further information failed)")
                        .into_raw(),
                    contents: null(),
                }
            )
        )
    };

    if debug {
        println!("[libhaskell_exceptionfree_readfile] Successfully converted path, reading contents...");
    }

    // Read the file into a string
    let contents = match std::fs::read_to_string(path) {
        Ok(v) => v,
        Err(e) => return Box::into_raw(
            Box::new(
                ContentsOrError {
                    err: CString::new(format!("Failed to read file: {}", e))
                        .expect("read_to_string failed (printing futher information failed")
                        .into_raw(),
                    contents: null(),
                }
            )
        )
    };

    if debug {
        println!("[libhaskell_exceptionfree_readfile] Building CString from contents...");
    }

    // Build a CString from the contents
    let cstr_contents = match CString::new(contents) {
        Ok(v) => v,
        Err(e) => return Box::into_raw(
            Box::new(
                ContentsOrError {
                    err: CString::new(format!("Failed to convert string: {}", e))
                        .expect("building comment string fialed (printing further information failed)")
                        .into_raw(),
                    contents: null(),
                }
            )
        )
    };

    if debug {
        println!("[libhaskell_exceptionfree_readfile] Returning struct with contents or error...");
        println!("[libhaskell_exceptionfree_readfile] address [{:p}]", cstr_contents.as_ptr())
    }

    return Box::into_raw(
        Box::new(
            ContentsOrError { err: null(), contents: cstr_contents.into_raw() }
        )
    )
}

#[cfg(test)]
mod tests {
    #[test]
    fn basic_file_read_works() {
        let contents = read_file("../test/fixtures/example.txt").unwrap();
        assert_eq!(contents, "This is a test file\n");
    }
}
