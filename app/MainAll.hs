{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Version (showVersion)
import Paths_exceptionfree_readfile (version)
import System.Environment (getArgs)
import qualified System.IO (readFile)
import qualified System.IO.ExceptionFree as ExceptionFree
import qualified System.IO.ExceptionFree.Internal.Oxidized as Oxidized

data Mode = ExceptionFree
          | Original
          | Oxidized
            deriving (Eq, Enum, Show)

readMode :: String -> Maybe Mode
readMode "ExceptionFree" = Just ExceptionFree
readMode "exception-free" = Just ExceptionFree
readMode "Original" = Just Original
readMode "original" = Just Original
readMode "Oxidized" = Just Oxidized
readMode "oxidized" = Just Oxidized
readMode _ = Nothing

parseArgs :: [String] -> IO ()
parseArgs ["-h"]                 = printUsage
parseArgs ["--help"]             = printUsage
parseArgs ["-v"]                 = printVersion
parseArgs ["--version"]          = printVersion
parseArgs ["-m", mode, path]     = parseMode mode
                                   >>= doReadFile path
parseArgs ["--mode", mode, path] = parseMode mode
                                   >>= doReadFile path
parseArgs _                      = printUsage

parseMode :: String -> IO Mode
parseMode s = case readMode s of
                Nothing -> putStrLn ("[error] Invalid mode [" <> s <> "], valid modes are 'exception-free', 'original', and 'oxidized'\n")
                           >> printUsage
                           >> putStrLn ""
                           >> error "Invalid mode"
                Just parsedMode  -> pure parsedMode

main :: IO ()
main = getArgs >>= parseArgs

printVersion :: IO ()
printVersion = putStrLn $ showVersion version

printUsage :: IO ()
printUsage = putStrLn "Usage: exceptionfree-readfile [-h|--help] [-v|--version] [-m|--mode] <filename>"
             >> putStrLn "exceptionfree-readfile prints the contents of a single file to stdout"
             >> putStrLn "mode := [exception-free, original, oxidized]"

doReadFile :: String -> Mode -> IO ()
doReadFile path mode = putStrLn ("Reading file [" <> path <> "]")
                       >> case mode of
                            ExceptionFree -> ExceptionFree.readFile path
                            Original -> Right <$> readFile path
                            Oxidized -> Oxidized.readFile path
                       >>= \res -> case res of
                                     Left err -> putStrLn ("Error: " <> show err)
                                     Right contents -> putStrLn contents
