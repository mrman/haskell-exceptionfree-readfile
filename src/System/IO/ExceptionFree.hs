{-# LANGUAGE NoImplicitPrelude #-}

module System.IO.ExceptionFree ( readFile, FileContents ) where

import qualified System.IO.ExceptionFree.Internal.Custom  as Custom

type FileContents = Custom.FileContents
readFile = Custom.readFile
