{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module System.IO.ExceptionFree.Internal.Oxidized ( readFile
                                                 , readFileDebug
                                                 ) where

import Data.Monoid ((<>))
import Control.Applicative (pure)
import Control.Exception (Exception)
import Control.Monad ((>>=), (>>), return)
import Data.Bool ((&&), Bool(..))
import Data.Either (Either(..))
import Data.Eq (Eq, (==))
import Data.Function (($), (.))
import Data.Int (Int)
import Data.Maybe (Maybe(..))
import Data.String (String)
import Foreign.C (CString, newCString, peekCString)
import Foreign.Ptr (Ptr, nullPtr)
import Foreign.Storable (Storable(..))
import GHC.Err (undefined)
import GHC.IO.Exception (IOException(..), IOError(..), IOErrorType(..))
import GHC.Num ((*))
import GHC.Show (Show(..))
import System.IO (FilePath, IO(..), putStrLn)

type DebugState = Bool

-- | Type that comes back from the rust side
data ContentsOrError = ContentsOrError { err      :: CString
                                       , contents :: CString
                                       } deriving (Eq, Show)

-- | Pointer to a ContentsOrError struct
type ContentsOrErrorPtr = Ptr ContentsOrError

-- | Size of a cstring (this could be 32/64 bytes?)
cstrSize :: Int
cstrSize = sizeOf (undefined :: CString)

instance Storable ContentsOrError where
    -- | ContentsOrError contains two nullable pointers (to character sequences), each expected to be 4 bytes
    sizeOf _ = 2 * sizeOf (undefined :: CString)
    -- | Aligned on the size of one of the nullable pointers
    alignment _ = sizeOf (undefined :: CString)
    -- | Peek bytes for the nullable pointers one by one and re-form
    peek ptr = peekByteOff ptr 0
               >>= \err -> peekByteOff ptr cstrSize
               >>= \contents -> pure (ContentsOrError err contents)
    -- | Poke bytes for the nullable pointers into memory
    poke ptr (ContentsOrError err contents) = pokeByteOff ptr cstrSize err
                                              >> pokeByteOff ptr cstrSize contents

foreign import ccall "read_file" read_file :: CString -> Bool -> IO ContentsOrErrorPtr

readFailedErr :: String -> IOError
readFailedErr str = IOError Nothing IllegalOperation "read_file" str Nothing Nothing

unexpectedFailureError :: IOError
unexpectedFailureError = IOError Nothing IllegalOperation "read_file" "unexpected failure" Nothing Nothing

ffiResultParseFailureError :: IOError
ffiResultParseFailureError = IOError Nothing IllegalOperation "read_file" "FFI result parse (`peek`) failure" Nothing Nothing

readFile :: FilePath -> IO (Either IOError String)
readFile = _readFile False

readFileDebug :: FilePath -> IO (Either IOError String)
readFileDebug = _readFile True

_readFile :: DebugState -> FilePath -> IO (Either IOError String)
_readFile debugState path = newCString path
                >>= \pathCStr -> read_file pathCStr debugState
                >>= peek
                >>= \case
                        (ContentsOrError err contents)
                          -- | Unsuccessful read without error - unexpected failure
                          | err == nullPtr && contents == nullPtr -> pure $ Left unexpectedFailureError
                          -- | Unsuccessful read with error - contents missing but err was not null
                          | contents == nullPtr                   -> peekCString err
                                                                     >>= pure . Left . readFailedErr
                          -- | Successful read with no error - was null and contents weren't
                          | err == nullPtr                        -> peekCString contents
                                                                     >>= pure . Right
                        -- | If we don't get the parse we expected then
                        _ -> pure $ Left ffiResultParseFailureError
