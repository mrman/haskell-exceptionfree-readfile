# Changelog for `exceptionfree-readfile`

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased changes

## [0.1.0.0]  - 2020/01/28
### Added
- Base functionality for `readFile` that does not log exceptions
- Added benchmarks ([#6](https://gitlab.com/mrman/haskell-exceptionfree-readfile/issues/6))
- Add top-level `handle` for any unexpected errors

### Changed
- Added documentation hosting [on Gitlab pages](http://mrman.gitlab.io/exceptionfree-readfile)
- Isolated the oxidized code & build process ([#4](https://gitlab.com/mrman/haskell-exceptionfree-readfile/issues/4))
- Improved performance and safety ([#5](https://gitlab.com/mrman/haskell-exceptionfree-readfile/issues/5))

### Removed

## [0.0.1.0]  - 2020/01/28
### Added
- Base functionality for `readFile` that does not log exceptions

### Changed

### Removed

[Unreleased]: https://gitlab.com/mrman/haskell-exceptionfree-readfile/compare/v0.0.1.0...HEAD
[0.0.1.0]: https://gitlab.com/mrman/haskell-exceptionfree-readfile/tree/v0.0.1.0
