.PHONY: all lint lint-watch print-release-version clean docs publish \
				check-tool-cargo \
				build build-watch build-test-unit-watch build-test-e2e-watch \
				install \
				test test-all test-unit test-e2e \
				bench bench-gen-results \
				ensure-target-bin \
				builder-base-image registry-login publish-builder-base \
				test-all test-smoke test-example-file test-example-long-file \
				rust rust-lib-debug rust-lib-release rust-clean \
				setup-cabal-dist-file revert-cabal-dist-file dist publish

all: lint build install

STACK ?= stack
HLINT ?= hlint
DOCKER ?= docker
ENTR ?= entr
CARGO ?= cargo

PROJECT_NAME = exceptionfree-readfile
TARGET_DIR ?= target
TARGET_BIN_PATH ?= target/$(PROJECT_NAME)
RUST_SO_PATH_DEBUG ?= $(shell realpath rust/target/debug/libhaskell_exceptionfree_readfile.so)
RUST_SO_PATH_RELEASE ?= $(shell realpath rust/target/release/libhaskell_exceptionfree_readfile.so)
RUST_SO_DIR_RELEASE ?= $(shell realpath rust/target/release)
STACK_INSTALL_OPTS ?=

ifeq ($(OXIDIZED), true)
	STACK_BUILD_OPTS ?= --profile --local-bin-path $(TARGET_DIR) --copy-bins --ghc-options="-l$(RUST_SO_PATH_RELEASE)"
else
	STACK_BUILD_OPTS ?= --profile --local-bin-path $(TARGET_DIR) --copy-bins
endif

ifeq ($(OXIDIZED), true)
	STACK_TEST_OPTS ?= --ghc-options="-l$(RUST_SO_PATH_RELEASE)"
endif

ifeq ($(OXIDIZED), true)
	STACK_TEST_BUILD_OPTS ?= --ghc-options="-l$(RUST_SO_PATH_RELEASE)"
endif

ifeq ($(OXIDIZED), true)
	STACK_BENCH_OPTS ?= --ghc-options="-l$(RUST_SO_PATH_RELEASE)"
endif

METHOD ?= exception-free # exception-free, original, oxidized

OXIDIZED ?= false
OXIDIZED_FLAG ?=

ifeq ($(OXIDIZED), true)
	OXIDIZED_FLAG=--flag exceptionfree-readfile:oxidized
endif

VERSION ?= $(shell awk '/^version:\s+([0-9\.]+)/{print $$2}' exceptionfree-readfile.cabal)

# Ensure the target binary exists, building otherwise
ensure-target-bin:
ifeq (,$(wildcard ${TARGET_BIN_PATH}))
	@echo "[WARNING] missing target binary, building..."
	$(MAKE) build
endif

print-release-version:
	@echo -n $(VERSION)

lint:
	$(HLINT) src/ test/

lint-watch:
	find src/ test/ -name "*.hs" | $(ENTR) -rc $(HLINT) src/ test/

build:
	$(STACK) build $(STACK_BUILD_OPTS) $(OXIDIZED_FLAG)

build-watch:
	$(STACK) build $(STACK_BUILD_OPTS) --file-watch $(OXIDIZED_FLAG)

build-test-unit-watch:
	$(STACK) build $(STACK_TEST_BUILD_OPTS) $(OXIDIZED_FLAG) :unit --file-watch

build-test-e2e-watch:
	$(STACK) build $(STACK_TEST_BUILD_OPTS) $(OXIDIZED_FLAG) :e2e --file-watch

install:
	$(STACK) install $(STACK_INSTALL_OPTS) $(OXIDIZED_FLAG)

test: test-unit test-e2e

test-unit:
	$(STACK) test $(STACK_TEST_OPTS) $(OXIDIZED_FLAG) :unit

test-e2e: build
	$(STACK) test $(STACK_TEST_OPTS) $(OXIDIZED_FLAG) :e2e

# You can generate the benchmark files with:
# `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 1000 > bench/fixtures/example-KB1.txt`
bench: build
	$(STACK) bench $(STACK_BENCH_OPTS) $(OXIDIZED_FLAG)

bench-gen-results:
	$(STACK) bench $(STACK_BENCH_OPTS) $(OXIDIZED_FLAG) 2> bench/most-recent-results.txt

# You can generate the benchmark files with:
profile: build
	@echo "Running MB10 test for profiling..."
	$(TARGET_BIN_PATH) -m exception-free bench/fixtures/example-MB10.txt +RTS -p > /dev/null

clean: rust-clean
	$(STACK) clean

##########
# Docker #
##########

CONTAINER_NAME ?= haskell-exceptionfree-readfile
IMAGE_NAME_BUILDER_BASE ?= $(CONTAINER_NAME)-builder-base

REGISTRY_IMAGE_NAME ?= registry.gitlab.com/mrman/haskell-exceptionfree-readfile
REGISTRY_IMAGE_NAME_RELEASE ?= $(REGISTRY_IMAGE_NAME)/$(CONTAINER_NAME):$(VERSION)
REGISTRY_IMAGE_NAME ?= $(REGISTRY_IMAGE_NAME_RELEASE)$(GIT_SHA_SUFFIX) # add the SHA suffix if present
REGISTRY_IMAGE_NAME_BUILDER_BASE ?= $(REGISTRY_IMAGE_NAME)/$(IMAGE_NAME_BUILDER_BASE):latest

# Image for use with CI
builder-base-image:
	$(DOCKER) build -f infra/docker/builder-base.Dockerfile -t $(REGISTRY_IMAGE_NAME_BUILDER_BASE) .

registry-login:
	$(DOCKER) login registry.gitlab.com

publish-builder-base: registry-login
	$(DOCKER) push $(REGISTRY_IMAGE_NAME_BUILDER_BASE)

###################
# Utility targets #
###################

test-all: test test-smoke test-example-file test-example-long-file
	@echo -e "\nRunning rust tests..."
	$(MAKE) rust test-unit test-e2e OXIDIZED=true

test-smoke: test-example-file test-example-long-file
	$(MAKE) test-example-file

test-example-file: ensure-target-bin
	$(TARGET_BIN_PATH) -m $(METHOD) test/fixtures/example.txt

test-example-long-file: ensure-target-bin
	$(TARGET_BIN_PATH) -m $(METHOD) test/fixtures/example-long-file.txt

########
# Rust #
########

check-tool-cargo:
	@which $(CARGO) > /dev/null || \
	(echo -e "\n[error] Please install cargo (https://cargo.readthedocs.io)" && exit 1)

rust: check-tool-cargo rust-lib-debug rust-lib-release

rust-lib-debug:
	cd rust && \
	$(CARGO) build

rust-lib-release:
	cd rust && \
	$(CARGO) build --release

rust-clean:
	cd rust && \
	$(CARGO) clean

#################
# Documentation #
#################

DOCS_DIR=docs

docs:
	$(STACK) haddock
	cp -r .stack-work/dist/**/**/doc/html/$(PROJECT_NAME) docs/

################
# Distribution #
################

setup-cabal-dist-file:
	@echo "=> Switching .cabal files for dist..."
	@cp $(PROJECT_NAME).cabal $(PROJECT_NAME).cabal.bak
	@cp $(PROJECT_NAME).cabal.dist $(PROJECT_NAME).cabal

revert-cabal-dist-file:
	@echo "=> Resetting .cabal to add executable section..."
	@cp $(PROJECT_NAME).cabal.bak $(PROJECT_NAME).cabal
	@rm -f $(PROJECT_NAME).cabal.bak

dist:
	$(MAKE) setup-cabal-dist-file
	@echo "=> Running '$(STACK) sdist'..."
	$(STACK) sdist
	$(MAKE) revert-cabal-dist-file

publish:
	$(MAKE) setup-cabal-dist-file
	@echo "=> Publishing package (v$(VERSION)) w/ '$(STACK) upload'..."
	$(STACK) upload .
	$(MAKE) revert-cabal-dist-file
