{-# LANGUAGE OverloadedStrings #-}

module Fixtures where

-- | Default path to the generated binary for this project
defaultBinPath :: String
defaultBinPath = "target/exceptionfree-readfile"

-- | Path to the simple file fixture
simpleFilePath :: FilePath
simpleFilePath = "test/fixtures/example.txt"

-- | Simple file name template used when generating files
simpleFileNameTemplate :: String
simpleFileNameTemplate = "tmp-test-file-.txt"

-- | Simple content used for generated file
simpleFileContent :: String
simpleFileContent = "Hello World!"

-- | Exception thrown when a file is read (even if the read succeeds)
exceptionString :: String
exceptionString = "*** Exception (reporting due to +RTS -xc):"
