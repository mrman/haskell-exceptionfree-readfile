{-# LANGUAGE OverloadedStrings #-}

module CustomUnitSpec (spec) where

import           Fixtures (simpleFileNameTemplate, simpleFileContent)
import           System.IO.ExceptionFree (FileContents)
import           System.IO.Temp (writeSystemTempFile)
import           Test.Hspec
import           Util (shouldBeRight)
import qualified System.IO.ExceptionFree as ExceptionFree

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "basic file read" $
       it "reads generated temp files properly" $ \_ -> writeSystemTempFile simpleFileNameTemplate simpleFileContent
                                                        >>= ExceptionFree.readFile
                                                        >>= shouldBeRight
                                                        >>= shouldBe simpleFileContent
