{-# LANGUAGE OverloadedStrings #-}

module OxidizedUnitSpec (spec) where

import           Fixtures (simpleFileNameTemplate, simpleFileContent)
import           System.IO.Temp (writeSystemTempFile)
import           Test.Hspec
import           Util (shouldBeRight)
import qualified System.IO.ExceptionFree.Internal.Oxidized as Oxidized

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "basic file read" $
       it "reads generated temp files properly" $ \_ -> writeSystemTempFile simpleFileNameTemplate simpleFileContent
                                                        >>= \fp -> Oxidized.readFile fp
                                                        >>= shouldBeRight
                                                        >>= shouldBe simpleFileContent
