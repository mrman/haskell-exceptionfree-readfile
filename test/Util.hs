module Util ( shouldBeRight ) where

import Data.Either (Either(..))

shouldBeRight :: Show e => Either e a -> IO a
shouldBeRight (Left err) = error $ "Unexpected Left value:" ++ show err
shouldBeRight (Right v)  = return v
